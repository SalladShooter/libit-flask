import { getApp } from "./firebase.js";
import { getAuth, signOut, onAuthStateChanged, getIdToken } from "https://www.gstatic.com/firebasejs/10.7.2/firebase-auth.js";

const defaultIndexFile = "index.html";
const defaultExtension = ".html"
const app = getApp();
const auth = getAuth(app);

const htmlServerRe = /^\/html\/([\w-=+]+)/;

function getCurrentUser() {
  return new Promise(resolve => {
    const unsub = onAuthStateChanged(auth, user => {
      unsub();
      resolve(user ?? null);
    });
  });
}

async function getLibDir(libName) {
  try {
    const root = await navigator.storage.getDirectory();
    const libs = await root.getDirectoryHandle("libs", { create: true });
    return await libs.getDirectoryHandle(libName);
  } catch {
    return null;
  }
}

/** 
* @param {File} file
*/
function readFile(file) {
  const reader = new FileReader();

  return /** @type {Promise<string>} */ (new Promise((resolve, reject) => {
    reader.onload = () => resolve(reader.result);
    reader.onerror = () => reject(reader.error);

    reader.readAsText(file);
  }));
}

/** 
* @param {FileSystemFileHandle | FileSystemDirectoryHandle} handle
* @returns {handle is FileSystemFileHandle}
*/
async function isFile(handle) {
  return handle.kind === "file";
}

/** 
* Get a file handle from a path and directory handle.
* @param {string[]} path
* @param {FileSystemDirectoryHandle} directoryHandle
* @returns {Promise<FileSystemFileHandle>}
*/
async function recursiveResolvePath(path, directoryHandle) {
  if (path.length === 0) return await directoryHandle.getFileHandle(defaultIndexFile);

  try {
    const nextItem = path.shift();

    // If this is the last item, it's probably a file, but it could be a directory
    if (path.length === 0) {
      /** @type {FileSystemFileHandle | FileSystemDirectoryHandle} */
      const handle = await Promise.any([
        directoryHandle.getFileHandle(nextItem),
        directoryHandle.getFileHandle(nextItem + defaultExtension),
        directoryHandle.getDirectoryHandle(nextItem)
      ]);

      if (isFile(handle)) return handle;
      else {
        // look for the index file
        return await handle.getFileHandle(defaultIndexFile);
      }
    }
    // else it MUST be a directory
    else {
      const nextDirectory = await directoryHandle.getDirectoryHandle(nextItem);
      return await recursiveResolvePath(path, nextDirectory);
    }
  } catch {
    return null;
  }
}

addEventListener("fetch", e => e.respondWith((async () => {
  const url = new URL(e.request.url);

  // Remote requests
  if (url.origin !== location.origin) return await fetch(e.request);

  const htmlServerMatch = url.pathname.match(htmlServerRe);
  if (htmlServerMatch) {
    const [full, encodedLibId] = htmlServerMatch;
    const pathParts = url.pathname.replace(full, "").split("/").filter(el => el);
    const libDir = await getLibDir(encodedLibId);
    const handle = await recursiveResolvePath(pathParts, libDir);
    if (handle) {
      const file = await handle.getFile();
      return new Response(file);
    }
    else return new Response("404 Not Found", { status: 404 }); // TODO: custom 404
  }

  // Top level client
  const client = await clients.get(e.clientId);
  if (client) {
    const clientUrl = new URL(client.url);
    // Check if this is under a page that's on the HTML server currently
    const match = clientUrl.pathname.match(htmlServerRe);
    if (match) {
      const newUrl = match[0] + url.pathname;
      // Redirect it to the proper request with the lib prefix
      return new Response(null, { status: 302, headers: { "Location": newUrl } });
    }
  }

  // All other requests to our servers - add the firebase id token
  const user = await getCurrentUser();
  const idToken = user ? await getIdToken(user) : null;
  if (idToken) {
    const headers = new Headers(e.request.headers);
    if (!headers.has("Authorization")) headers.set("Authorization", `Bearer ${idToken}`);
    return await fetch(new Request(e.request, { headers }));
  }
  return await fetch(e.request);
})()));

addEventListener("message", e => e.waitUntil((async () => {
  const sourceUrl = new URL(e.source.url);
  // If it's coming from the HTML IDE, it's valid
  if (e.origin === location.origin && sourceUrl.pathname === "/ide-html.html" && e.data.type === "reload-html-libs" && e.data.lib) {
    // Reload all the clients with the given lib name
    const allClients = await clients.matchAll();
    await Promise.all(allClients.map(async client => {
      const url = new URL(client.url);
      if (url.pathname.startsWith(`/html/${e.data.lib}`)) await client.navigate(client.url);
    }));
  }

  // Sign out - can come from any page
  if (e.origin === location.origin && e.data.type === "sign-out") {
    // This exists so we don't have to load the firebase sdk just to sign out
    signOut(auth);
  }
})()));