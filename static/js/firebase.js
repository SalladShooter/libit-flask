// TODO: use a bundler?
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.7.2/firebase-app.js";

const config = {
  apiKey: "AIzaSyB8f9lsf9q8eGr2yRF9ZbSM5GLphDfZ3Wg",
  authDomain: "libit-main.firebaseapp.com",
  projectId: "libit-main",
  storageBucket: "libit-main.appspot.com"
};

export function getApp() {
  return initializeApp(config);
}