import os

from functools import wraps

from flask import Flask, redirect, render_template, request, g
from flask.helpers import url_for

import jwt  # upm package(pyjwt)
from jwt import PyJWKClient # upm package(pyjwt)
from jwt.exceptions import InvalidTokenError, PyJWKClientError # upm package(pyjwt)

from google.auth.transport.requests import AuthorizedSession  # upm package(google-auth)
from google.oauth2 import service_account  # upm package(google-auth)

app = Flask(__name__)
key = os.environ['SERVICE_ACCOUNT_KEY'].replace('\\n', '\n')
repl_key = os.environ['REPL_AUTH_PUBKEY'].replace('\\n', '\n')
project_id = 'libit-main'
email = f'libit-auth@{project_id}.iam.gserviceaccount.com'

creds = service_account.Credentials.from_service_account_info({
  'project_id': project_id,
  'private_key': key,
  'client_email': email,
  'token_uri': 'https://oauth2.googleapis.com/token'
}, scopes=[
  'https://www.googleapis.com/auth/firebase.database',
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/identitytoolkit'
])

authed_session = AuthorizedSession(creds)
jwks_client = PyJWKClient("https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com")

@app.before_request
def handle_firebase_token():
  auth_header = request.headers.get('Authentication')
  if auth_header:
    token = auth_header.removeprefix('Bearer ')

    try:
      signing_key = jwks_client.get_signing_key_from_jwt(token)
      data = jwt.decode(
        token,
        signing_key.key,
        algorithms=['RS256'],
        audience=project_id,
        issuer=f'https://securetoken.google.com/{project_id}'
      )
      g.user = data
    except (PyJWKClientError, InvalidTokenError):
      # Do nothing - they're not logged in
      pass

def auth_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
      if 'user' not in g or g.user is None:
        return redirect(url_for('login', next=request.url))
      return f(*args, **kwargs)
  return decorated_function

def auth_forbidden(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
      if 'user' in g and g.user is not None:
        return redirect(request.args.get('url', url_for('home')))
      return f(*args, **kwargs)
  return decorated_function

@app.route('/')
@auth_forbidden
def index():
    return render_template('index.html')

@app.route('/login/')
@auth_forbidden
def login():
    return render_template('login.html')


@app.route('/signup/')
@auth_forbidden
def signup():
    return render_template('login.html', signup=True)

@app.route('/account/')
@auth_required
def account():
    return render_template('login.html', signup=True)


@app.route('/ide/')
def ide():
    return 'not yet', 503


@app.route('/~')
@auth_required
def home():
    return render_template('home.html')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=81)
